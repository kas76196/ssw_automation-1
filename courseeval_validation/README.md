# __Course Eval File Input Validator__


This program was designed to take in a .xlsx file, containing the following columns:
Student Name, Student ID, Course Name, Course ID, Course Call Number, Term/Semester, Instructor Name, and Instructor ID

as well as a .csv file with the following columns, in order:
UserID, Instructor ID, and Instructor Last Name

The file will then check every Instructor Name and Instructor ID in the .xlsx file, against their counterparts in the .csv file. 

It will create two output files:

1. [original .xlsx file name]-report.xlsx, which has records which has unmatched name or MyID in .csv file

2. [original .xlsx file name]-verified.xlsx, which hold the matched records and records with corrected Instructor Name and Instructor ID

&nbsp;
&nbsp;
## __Dependencies__:
Python 3.9
pip

This program relies on the following Python 3.9 modules which can be installed with 

```console
$ pip install [module_name]
```
If you have multiple version of python installed, use the following commands instead:
```console
$ pip3 install openpyxl
```
and
```console
$ pip3 install PyQt6
```

&nbsp;
&nbsp;
## __Usage__:
There are two possible use cases for this application:

__No-GUI__ - Must specify the columns that each variable is located in, indexed from [0-7]: 

```console
$ python3 main.py [.xlsx file] [.csv file] [Student Name Column] [Course ID Column] [Course Call Number Column] [Student MyID Column] [Course Name Column] [Instructor Name Column] [Instructor ID Column] [Term/Semester Column]
```
__GUI__ - 

```console
$ python3 main.py 
```
Then in the pop dialog, browse directory and choose the .csv file and .xlsx file, and then map the column with the field name by referring the .xlsx file.

&nbsp;
&nbsp;
## __Known Issues__
The .xlse should be checked before runing the program to make sure all fields has value for each record

~~There is currently no error handling for duplicate columns.~~

~~It does not yet correct errors that it finds.~~

~~Having ./ in the file name will cause the files to just be named "-verified.xlsx" and "-unmatched.xlsx"~~
