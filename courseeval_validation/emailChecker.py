import openpyxl
import emailHelper
import csv
import copy
from openpyxl.styles import Font
import os


def EmailChecker(originalPath, comparePath, studentName, courseID, courseNum,
        studentID, courseName, instrName, instrID, term,):

    # Opening file to be checked and creating array to store the information
    originalWorkbook = openpyxl.load_workbook(originalPath)
    originalSheet = originalWorkbook.active
    originalEmails = []

    # Creating list of emails in the Course Eval system.
    compareEmails = []

    # This adds the elements from each of the files into their respective arrays, removing duplicates from the csv.
    with open(comparePath, newline="") as compareCSV:
        reader = csv.reader(compareCSV, delimiter=",", quotechar="|")
        for row in reader:
            if not compareEmails.__contains__(row):
                compareEmails.append(row)

    # Converting the worksheet into an array
    size = 0
    for j in originalSheet.rows:
        originalEmails.append([])
        for k in j:
            originalEmails[size].append(k.value)
        size = size + 1

    # Removing the title headers and creating an array to store the verified and unmatched emails.
    compareEmails.pop(0)
    originalEmails.pop(0)

    verifiedEmails = []
    reportedEmails = []
    unmatchedEmails = []
    reportedAccounts = 0

    # This checks the original list against the list of instructors in the course eval system.
    for i in range(len(originalEmails)):
        reported = True
        for j in compareEmails:
            
            # This will determine if anything is incorrect about the current row, and if so what the error is.
            # If email is correct and name is wrong, replaces name, vice versa replaces email.
            # If both are wrong it will add it to the verified emails as-is and then add it to the report in full as well.
            if (originalEmails[i][int(instrID)] == j[1]) & (originalEmails[i][int(instrName)] == j[2]):
                reported = False
            elif originalEmails[i][int(instrID)] == j[1]:
                reported = False
                reportedAccounts = reportedAccounts + 1
                if not [originalEmails[i][int(instrName)], originalEmails[i][int(courseID)], "Unmatched Instructor Name", j[2]] in unmatchedEmails:
                    unmatchedEmails.append([originalEmails[i][int(instrName)], originalEmails[i][int(courseID)], "Unmatched Instructor Name", j[2]])

                originalEmails[i][int(instrName)] = j[2]
                originalEmails[i].append(
                    "Instructor ID was correct, Instructor Name was not.")
            elif originalEmails[i][int(instrName)] == j[2]:
                reported = False
                reportedAccounts = reportedAccounts + 1

                if not [originalEmails[i][int(instrID)], originalEmails[i][int(courseID)], "Unmatched Instructor ID", j[1]] in unmatchedEmails:
                    unmatchedEmails.append([originalEmails[i][int(instrID)], originalEmails[i][int(courseID)], "Unmatched Instructor ID", j[1]])


                originalEmails[i][int(instrID)] = j[1]
                originalEmails[i].append(
                    "Incorrect ID: The ID was replaced with this.")
                
        verifiedEmails.append(originalEmails[i])

        if reported:
            if len(originalEmails[i]) < 9:
                originalEmails[i].append("No match was found.")
            reportedEmails.append(originalEmails[i])
            reportedAccounts = reportedAccounts + 1
        recordsProcessed = i + 1
    
    #Printing results to console...
    print("Records Processed: ", recordsProcessed)
    print("Unmatched Records Found: ", reportedAccounts)

    # Creating a new excel workbook to store the correct items.
    verifiedWorkbook = openpyxl.Workbook()
    verifiedWorksheet = emailHelper.newFormattedWorkbook(verifiedWorkbook)

    # This adds each of the rows from the verified emails to the new workbook.
    # It is formatted like this due to the course eval requiring specific formatting.
    for row in range(verifiedEmails.__len__()):
        verifiedWorksheet.cell(row + 2, 1).value = verifiedEmails[row][int(studentName)]
        verifiedWorksheet.cell(row + 2, 2).value = verifiedEmails[row][int(courseID)]
        verifiedWorksheet.cell(row + 2, 3).value = verifiedEmails[row][int(courseNum)]
        verifiedWorksheet.cell(row + 2, 4).value = verifiedEmails[row][int(studentID)]
        verifiedWorksheet.cell(row + 2, 5).value = verifiedEmails[row][int(courseName)]
        verifiedWorksheet.cell(row + 2, 6).value = verifiedEmails[row][int(instrName)]
        verifiedWorksheet.cell(row + 2, 7).value = verifiedEmails[row][int(instrID)]
        verifiedWorksheet.cell(row + 2, 8).value = verifiedEmails[row][int(term)]

    # This saves the new workbook with the original name + "-Verified" in the original location of the file.
    originalFileName = originalPath.split(".")[0]
    if originalFileName == "":
        originalFileName = "." + originalPath.split(".")[1]
    verifiedFileLoc = originalFileName + "-Verified.xlsx"
    
    # YUE added output directory for output files 10/08/2021
    head, tail = os.path.split(verifiedFileLoc)
    newOutputLoc = head +  "/output/" + tail
    verifiedWorkbook.save(newOutputLoc)

    # verifiedWorkbook.save(verifiedFileLoc)

    # Creating a new workbook for the incorrect items and adding an extra row for the error that was encountered.
    header = Font(
        name="Calibri",
        size=14,
        bold=True,
    )

    reportWorkbook = openpyxl.Workbook()
    reportWorksheet = emailHelper.newResultWorkbook(reportWorkbook, len(unmatchedEmails) + 2)

    # This adds each of the rows from the incorrect emails to the new workbook.
    # It is formatted like this due to the course eval requiring specific formatting.
    for row in range(unmatchedEmails.__len__()):
        reportWorksheet.cell(row + 2, 1).value = unmatchedEmails[row][0]
        reportWorksheet.cell(row + 2, 2).value = unmatchedEmails[row][1]
        reportWorksheet.cell(row + 2, 3).value = unmatchedEmails[row][2]
        reportWorksheet.cell(row + 2, 4).value = unmatchedEmails[row][3]


    #This adds the emails that do not match either name or email to the end of the report.
    for row in range(len(unmatchedEmails), (reportedEmails.__len__() + len(unmatchedEmails))):
        reportWorksheet.cell(row + 3, 1).value = reportedEmails[row - len(unmatchedEmails)][int(studentName)]
        reportWorksheet.cell(row + 3, 2).value = reportedEmails[row - len(unmatchedEmails)][int(courseID)]
        reportWorksheet.cell(row + 3, 3).value = reportedEmails[row - len(unmatchedEmails)][int(courseNum)]
        reportWorksheet.cell(row + 3, 4).value = reportedEmails[row - len(unmatchedEmails)][int(studentID)]
        reportWorksheet.cell(row + 3, 5).value = reportedEmails[row - len(unmatchedEmails)][int(courseName)]
        reportWorksheet.cell(row + 3, 6).value = reportedEmails[row - len(unmatchedEmails)][int(instrName)]
        reportWorksheet.cell(row + 3, 7).value = reportedEmails[row - len(unmatchedEmails)][int(instrID)]
        reportWorksheet.cell(row + 3, 8).value = reportedEmails[row - len(unmatchedEmails)][int(term)]
        reportWorksheet.cell(row + 3, 9).value = reportedEmails[row - len(unmatchedEmails)][8]

    # This saves the unmatched emails to the original file location with "-Report" appended to the file name.
    reportFileLoc = originalFileName + "-Report.xlsx"
    # reportWorkbook.save(reportFileLoc)
    
     # YUE added output directory for output files 10/08/2021
    head2, tail2 = os.path.split(reportFileLoc)
    newOutputLoc2 = head2 +  "/output/" + tail2
    reportWorkbook.save(newOutputLoc2)