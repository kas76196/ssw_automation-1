from genericpath import exists
from emailChecker import EmailChecker
from window import Window
from os import path
import sys
from PyQt6.QtWidgets import QApplication


# If there are command-line arguments the Non-GUI version will launch, otherwise the GUI will launch.
if sys.argv.__len__() >= 2:
    originalFileSplit = sys.argv[1].split(".")
    originalFileExtension = originalFileSplit[len(originalFileSplit) - 1]
    compareFileSplit = sys.argv[2].split(".")
    compareFileExtension = compareFileSplit[len(compareFileSplit) - 1]

    if not path.exists(sys.argv[1]) or (originalFileExtension != "xlsx"):
        print("Incorrect input, please choose an existing .xlsx file")
        quit()
    if not path.exists(sys.argv[2]) or (compareFileExtension != "csv"):
        print("Incorrect input, please choose an existing .csv file")
        quit()

    if len(sys.argv) < 11:
        print("Incorrect input, too few input variables.")
        quit()

    for i in range(len(sys.argv) - 3):
        for j in range(len(sys.argv) - 3):
            if not (i + 3) == (j + 3):
                if sys.argv[i + 3] == sys.argv[j + 3]:
                    print(
                        "Incorrect Input, you have selected the same column for two variables."
                    )
                    quit()

    EmailChecker(
        sys.argv[1],
        sys.argv[2],
        sys.argv[3],
        sys.argv[4],
        sys.argv[5],
        sys.argv[6],
        sys.argv[7],
        sys.argv[8],
        sys.argv[9],
        sys.argv[10],
    )
else:
    app = QApplication(sys.argv)
    window = Window()
    window.show()
    sys.exit(app.exec())
