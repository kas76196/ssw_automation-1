#!/bin/sh
#
#
#
# pull latest/specified updates from jenkins site
############################################################
export http_proxy=http://webav.uga.edu:8080
update_version="$1"


if [ -z "$update_version" ]
then
   echo "Please specify a version to upgrade to"
   echo "USAGE:"
   echo "      $0 <version>"
   echo "      i.e. $0 2.150.2"
   echo
   echo " version URL: http://updates.jenkins-ci.org/download/war/"
fi
restart_jenkins="YES"
WARFILE="http://updates.jenkins-ci.org/download/war/${update_version}/jenkins.war"
LOCALWAR="/usr/share/jenkins/jenkins.war"
RC=0

cd /tmp
rm -f /tmp/jenkins.war

if ! $( echo "${update_version}" |  egrep "^[0-9]*\.[0-9].*$" >/dev/null 2>&1 )
then
   echo "Invalid update_version specified [ ${update_version} ], exiting..."
   exit 10
fi

wget $WARFILE
if [ $? -ne 0 ]
then
   echo "unable to download $WARFILE, exiting..."
   exit 20
fi

sudo cp $LOCALWAR  ${LOCALWAR}.$(date +'%Y%m%d')
if [ $? -ne 0 ]
then
   echo "unable to create backup for $LOCALWAR, exiting..."
   exit 21
fi

sudo mv /tmp/jenkins.war /usr/share/jenkins/.
if [ $? -ne 0 ]
then
   echo "unable to move downloaded war file to $LOCALWAR, exiting..."
   exit 22
fi

if $( echo "${restart_jenkins}" | grep "YES" >/dev/null 2>&1 )
then
   sudo service jenkins restart
   if [ $? -ne 0 ]
   then
      echo "unable to restart jenkins application, exiting..."
      exit 23
   fi
fi

exit $RC
