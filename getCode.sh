#!/bin/sh
#
#
# get code from bitbucket repositories
#####################################################
VERSION="1.0.7"
USAGE="
$0 <repository name> <branch name>
i.e. $0 'ssw_automation' 'master'
"

REPO_NAME=$1
BRANCH=$2
SSW_PROJECTS_DIR="/ssw_projects/git_repos"
LOCAL_DIR_NAME="$SSW_PROJECTS_DIR/${REPO_NAME}_${BRANCH}"

echo "GET CODE SCRIPT VERSION: $VERSION"
if [ ! -d "$SSW_PROJECTS_DIR" ]
then
   echo "ERROR: unable to find the ssw projects directory: $SSW_PROJECTS_DIR"
   exit 21
fi

if [ -z "$REPO_NAME" ]
then
   echo "Please enter a bitbucket repository name"
   echo "$USAGE"
   exit 31
fi

if [ -z "$BRANCH" ]
then
   echo "Please enter a bitbucket repository branch name"
   echo "$USAGE"
   exit 32 
fi


##################
# MAIN
##################
cd $SSW_PROJECTS_DIR
if [ $? -ne 0 ]
then
   echo "ERROR: unable to change directory to $SSW_PROJECTS_DIR"
   exit 101
fi

echo "delete existing directory $LOCAL_DIR_NAME"
sudo rm -rf $LOCAL_DIR_NAME

if [ ! -d "$LOCAL_DIR_NAME" ]
then 
   echo "executing git clone"
   git clone git@bitbucket.org:sswits/${REPO_NAME}.git $LOCAL_DIR_NAME -v
   if [ $? -ne 0 ]
   then
      echo "ERROR: unable to clone git@bitbucket.org:sswits/${REPO_NAME}.git to $LOCAL_DIR_NAME"
      exit 102
   fi
else
   echo "$LOCAL_DIR_NAME still exists and was not deleted properly, exiting..."
   exit 103
fi

cd $LOCAL_DIR_NAME
if [ $? -ne 0 ]
then
   echo "ERROR: unable to change directory to $LOCAL_DIR_NAME"
   exit 104
fi
pwd

echo "git checkout from $BRANCH"
git checkout $BRANCH
if [ $? -ne 0 ]
then
   echo "ERROR: unable to git checkout from branch $BRANCH"
   exit 105
fi


echo "git pull (reset) from $BRANCH"
git pull origin $BRANCH
if [ $? -ne 0 ]
then
   git reset --hard origin/$BRANCH
   if [ $? -ne 0 ]
   then
      echo "ERROR: unable to git pull from $BRANCH"
      exit 106
   fi
fi

echo "change permissions for all bash scripts to be executable"
sudo find $LOCAL_DIR_NAME -name "*.sh" -exec chmod 755 {} \;


