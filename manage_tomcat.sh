#!/bin/sh
#
# stop / start / restart tomcat
##########################################

cmd=$1
export JAVA_HOME=/usr/lib/jvm/default-java/jre
#export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-amd64
export PATH=$JAVA_HOME/bin:$PATH

env | egrep -i "java|path"

if ! $( echo "$cmd" | egrep "stop|start|restart|status" >/dev/null 2>&1 )
then
   echo "Please enter an appropriate command: stop|start|restart|status"
   exit 10
fi

sudo service tomcat8 $cmd
echo $?
